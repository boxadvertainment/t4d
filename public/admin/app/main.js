System.register(['angular2/platform/browser', './app.component', 'angular2/router', 'angular2/http', './app.service', 'rxjs/Rx'], function(exports_1) {
    var browser_1, app_component_1, router_1, http_1, app_service_1;
    return {
        setters:[
            function (browser_1_1) {
                browser_1 = browser_1_1;
            },
            function (app_component_1_1) {
                app_component_1 = app_component_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (app_service_1_1) {
                app_service_1 = app_service_1_1;
            },
            function (_1) {}],
        execute: function() {
            browser_1.bootstrap(app_component_1.AppComponent, [
                router_1.ROUTER_PROVIDERS, http_1.HTTP_PROVIDERS, app_service_1.AppService
            ]);
        }
    }
});
// @ https://github.com/mgechev/angular2-seed
// import {provide, enableProdMode} from 'angular2/core';
// import {ROUTER_PROVIDERS, APP_BASE_HREF} from 'angular2/router';
// if ('<%= ENV %>' === 'prod') { enableProdMode(); }
// bootstrap(AppComponent, [
//   ROUTER_PROVIDERS,
//   provide(APP_BASE_HREF, { useValue: '<%= APP_BASE %>' })
// ]);
// @ https://github.com/springboot-angular2-tutorial/angular2-app
// if ('production' === process.env.ENV) {
//   enableProdMode();
// }  
//# sourceMappingURL=main.js.map