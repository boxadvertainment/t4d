System.register(['angular2/core'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var APP_START_TIME, AppService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            exports_1("APP_START_TIME", APP_START_TIME = 'APP_START_TIME');
            AppService = (function () {
                function AppService() {
                    this._state = {}; // you must set the initial value
                }
                // constructor(webpackState: WebpackState) {
                //     this._state = webpackState.select('AppState', () => this._state);
                // }
                AppService.prototype.get = function (prop) {
                    return this._state[prop] || this._state;
                };
                AppService.prototype.set = function (prop, value) {
                    return this._state[prop] = value;
                };
                AppService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [])
                ], AppService);
                return AppService;
            })();
            exports_1("AppService", AppService);
        }
    }
});
//# sourceMappingURL=app.service.js.map