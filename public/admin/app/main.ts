import {bootstrap}        from 'angular2/platform/browser';
//import {provide}          from 'angular2/core';
import {AppComponent}     from './app.component';
import {ROUTER_PROVIDERS} from 'angular2/router';
import {Http, HTTP_PROVIDERS}   from 'angular2/http';
import {AppService}       from './app.service';
//import {AuthConfig, AuthHttp} from 'angular2-jwt';
import 'rxjs/Rx';

bootstrap(AppComponent, [
  ROUTER_PROVIDERS, HTTP_PROVIDERS, AppService
//   , provide(AuthHttp, {
//       useFactory: (http) => {
//         return new AuthHttp(new AuthConfig({
//           tokenName: 'jwt'
//         }), http);
//       },
//       deps: [Http]
//     })
]);

// @ https://github.com/mgechev/angular2-seed
// import {provide, enableProdMode} from 'angular2/core';
// import {ROUTER_PROVIDERS, APP_BASE_HREF} from 'angular2/router';
// if ('<%= ENV %>' === 'prod') { enableProdMode(); }

// bootstrap(AppComponent, [
//   ROUTER_PROVIDERS,
//   provide(APP_BASE_HREF, { useValue: '<%= APP_BASE %>' })
// ]);


// @ https://github.com/springboot-angular2-tutorial/angular2-app
// if ('production' === process.env.ENV) {
//   enableProdMode();
// } 