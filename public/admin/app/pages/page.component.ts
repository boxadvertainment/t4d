import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';
import {PageListComponent}   from './page-list.component';
import {PageDetailComponent} from './page-detail.component';
import {PageService}         from './page.service';

@Component({
  templateUrl: './app/app.html',
  directives: [ROUTER_DIRECTIVES],
  providers:  [PageService]
})
@RouteConfig([
  {path:'/',         name: 'PagesList', component: PageListComponent, useAsDefault: true},
  {path:'/:id',      name: 'PageDetail', component: PageDetailComponent}
])
export class PageComponent { }
