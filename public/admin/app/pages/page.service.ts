import {Injectable} from 'angular2/core';
import {Http} from 'angular2/http';
import {Observable}     from 'rxjs/Observable';

export class Page {
  constructor(public id: number, public name: string) { }
}
@Injectable()
export class PageService {
  constructor(private http: Http) {
  }
  getPages() {
    return this.http.get('http://starter-site.dev/admin/api/page')
                    .map(res => res.json());
                    // .catch(this.handleError);
  }
  getPage(id: number | string) {
    return this.http.get('http://starter-site.dev/admin/api/page/' + id)
                    .map(res => res.json());
  }
  updatePage(page: Page) {
    let body = JSON.stringify(page);
    return this.http.put('http://starter-site.dev/admin/api/page/' + page.id, body)
                    .map(res => res.json());
  }
  private handleError (error) {
    // in a real world app, we may send the error to some remote logging infrastructure
    // instead of just logging it to the console
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }
}
