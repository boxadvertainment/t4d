System.register(['angular2/core', 'angular2/router', './page-list.component', './page-detail.component', './page.service'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, page_list_component_1, page_detail_component_1, page_service_1;
    var PageComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (page_list_component_1_1) {
                page_list_component_1 = page_list_component_1_1;
            },
            function (page_detail_component_1_1) {
                page_detail_component_1 = page_detail_component_1_1;
            },
            function (page_service_1_1) {
                page_service_1 = page_service_1_1;
            }],
        execute: function() {
            PageComponent = (function () {
                function PageComponent() {
                }
                PageComponent = __decorate([
                    core_1.Component({
                        templateUrl: './app/app.html',
                        directives: [router_1.ROUTER_DIRECTIVES],
                        providers: [page_service_1.PageService]
                    }),
                    router_1.RouteConfig([
                        { path: '/', name: 'PagesList', component: page_list_component_1.PageListComponent, useAsDefault: true },
                        { path: '/:id', name: 'PageDetail', component: page_detail_component_1.PageDetailComponent }
                    ]), 
                    __metadata('design:paramtypes', [])
                ], PageComponent);
                return PageComponent;
            })();
            exports_1("PageComponent", PageComponent);
        }
    }
});
//# sourceMappingURL=page.component.js.map