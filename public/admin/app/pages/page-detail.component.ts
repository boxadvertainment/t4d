import {Component, OnInit}  from 'angular2/core';
import {Page, PageService} from './page.service';
import {RouteParams, Router} from 'angular2/router';

declare var jQuery: any;

@Component({
  templateUrl: './app/pages/page-detail.html',
})
export class PageDetailComponent {
  public page: Page;
  constructor(
    private _service: PageService,
    private _router: Router,
    private _routeParams: RouteParams
    ) { }
    

  ngOnInit() {
    let id = +this._routeParams.get('id');
    this._service.getPage(id).subscribe(page => {
      if (page) {
          //setTimeout(()=> {this.page = page}, 2000);
          
        this.page = page;
        jQuery('.summernote').summernote();
      } else { // id not found
        this.gotoPages();
      }
    });
  }
  
  ngAfterViewChecked() {
      jQuery('.summernote').summernote();
  }

  gotoPages() {
    this._router.navigate(['Pages']);
  }
  save() {
    console.log(this.page);
    this._service.updatePage(this.page).subscribe(res => {
      alert('Enregistrement avec success !!');
    });
  }
}
