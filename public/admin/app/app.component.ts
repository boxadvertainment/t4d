import {Component}                 from 'angular2/core';
import {RouteConfig, RouterOutlet}               from 'angular2/router';
import {LoggedInRouterOutlet}      from './login/LoggedInOutlet';
import {DashboardComponent}        from './dashboard/dashboard.component';
import {PageComponent}             from './pages/page.component';
import {LoginComponent}            from './login/login.component';
// import {AuthHttp} from 'angular2-jwt';
// import {tokenNotExpired} from 'angular2-jwt';
//

@Component({
    selector: 'my-app',
    template: `<router-outlet></router-outlet>`,
    directives: [LoggedInRouterOutlet]
})
@RouteConfig([
  {path: '/dashboard', name: 'Dashboard', component: DashboardComponent, useAsDefault: true},
  {path: '/login', name: 'Login', component: LoginComponent},
  {path: '/pages/...', name: 'Pages', component: PageComponent}
  //{path: '/**', redirectTo: ['404']}
])
//

export class AppComponent {
    name: string = 'RoboxCMS';
    url: string = 'http://starter-site.dev';
    someClass: boolean = true;
}
