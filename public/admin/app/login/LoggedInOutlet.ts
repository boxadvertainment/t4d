import { ElementRef, DynamicComponentLoader, AttributeMetadata, Directive } from 'angular2/core';
import { Router, RouterOutlet } from 'angular2/router';
//import { AuthService } from './auth.service';

@Directive({
  selector: 'router-outlet'
})
export class LoggedInRouterOutlet extends RouterOutlet {
  private parentRouter: Router;
  publicRoutes = ['login', 'signup'];

  static get parameters() {
    return [[ElementRef], [DynamicComponentLoader], [Router], [new AttributeMetadata('name'), String]];
  }

  constructor(elementRef, componentLoader, parentRouter, name) {
    super(elementRef, componentLoader, parentRouter, name);

    this.parentRouter = parentRouter;
    //this.authService = AuthService;
  }

  activate(instruction) {
    if (this._canActivate(instruction.urlPath)) {
      return super.activate(instruction);
    }

    this.parentRouter.navigate(['Login']);
  }

  _canActivate(url) {
    //return this.publicRoutes.indexOf(url) !== -1 || this.authService.isLoggedIn();
    return this.publicRoutes.indexOf(url) !== -1 || false;
  }
}