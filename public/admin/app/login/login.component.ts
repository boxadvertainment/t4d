import {Component, HostBinding} from 'angular2/core';
import {FORM_DIRECTIVES, FormBuilder,  ControlGroup, NgIf} from 'angular2/common';
import {Router} from 'angular2/router';
import { Http, Headers } from 'angular2/http';
//import {Authentication} from './authentication.service';


@Component({
  templateUrl: './app/login/login.html',
  directives: [ FORM_DIRECTIVES, NgIf ]
})

export class LoginComponent {
  form: ControlGroup;
  error: boolean = false;
  someClass: boolean = true;
  //constructor(fb: FormBuilder,  public router: Router) {
    // this.form = fb.group({
    //   username:  ['', Validators.required],
    //   password:  ['', Validators.required]
    // });
  //}

  onSubmit(value: any) {
    // this.auth.login(value.username, value.password)
    //   .subscribe(
    //     (token: any) => this.router.navigate(['../Home']),
    //     () => { this.error = true; }
    //   );
  }
  
  constructor(public router: Router, public http: Http) {
  }
  
  login(event, username, password) {
    event.preventDefault();
    let body = JSON.stringify({ username, password });
    
    let contentHeaders = new Headers();
    contentHeaders.append('Accept', 'application/json');
    contentHeaders.append('Content-Type', 'application/json');
    
    this.http.post('http://localhost:3001/sessions/create', body, { headers: contentHeaders })
      .subscribe(
        response => {
          localStorage.setItem('jwt', response.json().id_token);
          this.router.parent.navigateByUrl('/home');
        },
        error => {
          alert(error.text());
          console.log(error.text());
        }
      );
  }
}

//const toastr = require('toastr/toastr');
// export class LoginPage {
//   constructor(private router:Router,
//               private loginService:LoginService) {
//   }

//   login(email, password) {
//     this.loginService.login(email, password)
//       .subscribe(() => {
//         this.router.navigate(['/Home']);
//       }, this.handleError)
//     ;
//   }

//   handleError(error) {
//     switch (error.status) {
//       case 401:
//         toastr.error('Email or password is wrong.');
//     }
//   }

// }