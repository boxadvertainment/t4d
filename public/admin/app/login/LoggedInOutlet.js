System.register(['angular2/core', 'angular2/router'], function(exports_1) {
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1;
    var LoggedInRouterOutlet;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }],
        execute: function() {
            //import { AuthService } from './auth.service';
            LoggedInRouterOutlet = (function (_super) {
                __extends(LoggedInRouterOutlet, _super);
                function LoggedInRouterOutlet(elementRef, componentLoader, parentRouter, name) {
                    _super.call(this, elementRef, componentLoader, parentRouter, name);
                    this.publicRoutes = ['login', 'signup'];
                    this.parentRouter = parentRouter;
                    //this.authService = AuthService;
                }
                Object.defineProperty(LoggedInRouterOutlet, "parameters", {
                    get: function () {
                        return [[core_1.ElementRef], [core_1.DynamicComponentLoader], [router_1.Router], [new core_1.AttributeMetadata('name'), String]];
                    },
                    enumerable: true,
                    configurable: true
                });
                LoggedInRouterOutlet.prototype.activate = function (instruction) {
                    if (this._canActivate(instruction.urlPath)) {
                        return _super.prototype.activate.call(this, instruction);
                    }
                    this.parentRouter.navigate(['Login']);
                };
                LoggedInRouterOutlet.prototype._canActivate = function (url) {
                    //return this.publicRoutes.indexOf(url) !== -1 || this.authService.isLoggedIn();
                    return this.publicRoutes.indexOf(url) !== -1 || false;
                };
                LoggedInRouterOutlet = __decorate([
                    core_1.Directive({
                        selector: 'router-outlet'
                    }), 
                    __metadata('design:paramtypes', [Object, Object, Object, Object])
                ], LoggedInRouterOutlet);
                return LoggedInRouterOutlet;
            })(router_1.RouterOutlet);
            exports_1("LoggedInRouterOutlet", LoggedInRouterOutlet);
        }
    }
});
//# sourceMappingURL=LoggedInOutlet.js.map