import {Injectable} from 'angular2/core';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AuthService {
  token: string;

  constructor() {
    this.token = localStorage.getItem('token');
  }

  login(username: String, password: String) {
    /*
     * If we had a login api, we would have done something like this

    return this.http.post('/auth/login', JSON.stringify({
        username: username,
        password: password
      }), {
        headers: new Headers({
          'Content-Type': 'application/json'
        })
      })
      .map((res : any) => {
        let data = res.json();
        this.token = data.token;
        localStorage.setItem('token', this.token);
      });

      for the purpose of this cookbook, we will juste simulate that
    */

    if (username === 'test' && password === 'test') {
      this.token = 'token';
      localStorage.setItem('token', this.token);
      return Observable.of('token');
    }

    return Observable.throw('authentication failure');
  }

  logout() {
    /*
     * If we had a login api, we would have done something like this

    return this.http.get(this.config.serverUrl + '/auth/logout', {
      headers: new Headers({
        'x-security-token': this.token
      })
    })
    .map((res : any) => {
      this.token = undefined;
      localStorage.removeItem('token');
    });
     */

    this.token = undefined;
    localStorage.removeItem('token');

    return Observable.of(true);
  }
  
  isLoggedIn() {
      return false;
  }
  
  //https://github.com/born2net/ng2Boilerplate/blob/master/src/services/AuthService.ts
//   public checkAccess(to:ComponentInstruction, from:ComponentInstruction, target = ['/Login/Login']):Promise<any> {
//         let injector:Injector = appInjService();
//         let router:Router = injector.get(Router);

//         // since this is just an example app we will always set m_authenticated to true, but
//         // here you will want to add some server side auth logic
//         // set value to false to see what happens when a user auth is rejected
//         // and the router will default back to the login screen... cool stuff!
//         // this.m_authenticated = false;
//         this.m_authenticated = true;
//         if (this.m_authenticated)
//             return Promise.resolve(true);

//         // here is an example of auth that failed and we nabigate to default route
//         // and resolve false so a calling component's @CanActivate will not load
//         if (!this.m_authenticated) {
//             bootbox.alert('Sorry user or password are incorrect');
//             router.navigate(target);
//             return Promise.resolve(false);
//         }

//         // some more sample code of possibilities
//         // return new Promise((resolve) => {
//         //     var credentials = this.localStorage.getItem('remember_me');
//         //     var user = credentials.u;
//         //     var pass = credentials.p;
//         //     var remember = credentials.r;
//         //
//         //     this.appdbAction.createDispatcher(this.appdbAction.authenticateUser)(user, pass, remember);
//         //
//         //     this.m_pendingNotify = (status) => {
//         //         resolve(status);
//         //         if (!status) {
//         //             router.navigate(target);
//         //             resolve(false);
//         //         }
//         //     }
//         // });
//     }
}