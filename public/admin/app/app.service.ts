import {Injectable} from 'angular2/core';

export const APP_START_TIME = 'APP_START_TIME';

@Injectable()
export class AppService {
    _state = {}; // you must set the initial value
    
    // constructor(webpackState: WebpackState) {
    //     this._state = webpackState.select('AppState', () => this._state);
    // }
    get(prop?: any) {
        return this._state[prop] || this._state;
    }

    set(prop: string, value: any) {
        return this._state[prop] = value;
    }
}