var elixir = require('laravel-elixir');

require('./elixir-extensions');

elixir(function(mix) {
    mix
        /**
         * Copying third-party libraries
         */
        //.copy('node_modules/font-awesome/fonts', 'public/fonts')
        // --------- Creating scss files for plugins to using it in main.scss ---------
        //.copy('node_modules/animate.css/animate.css', 'node_modules/animate.css/scss/animate.scss')

        .sass('main.scss')
        //.browserify('main.js')
        .copy('resources/assets/img', 'public/img')
        .browserSync({
            proxy: process.env.APP_URL
        });

    if (elixir.config.production) {
        mix
            // Commented until they remove 'build' directory when using 'version' task
            //.version(['css/main.css', 'js/main.js'])
            .images();
    }
});
