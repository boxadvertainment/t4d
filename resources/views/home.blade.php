@extends('layout')

@section('class', 'home')

@section('content')
    <section class="a-propos-block text-center">
        <div class="container">
            <div class="text-block col-md-6">
                <h2>Qui sommes-nous</h2>
                <p>Tamkeen for Development « T4D » est une organisation non gouvernementale œuvrant dans la Capacitation Economique des populations vulnérables..</p>
                <a href="/a-propos" class="learn-more"> La suite </a>
            </div>
            <div class="pic-block col-md-6">
                <img src="{{ asset('img/about-pic.png') }}" alt="T4D">
            </div>
        </div>
    </section>
    <section class="mission-block">
        <div class="container">
            <div class="col-md-12 block">
                <div class="col-md-6">
                    <h2>Nos Missions</h2>
                    <p>Notre mission est de participer dans l’établissement d’une société civile où les jeunes et les femmes seront capables de se libérer financièrement et de participer activement dans le cycle économique et soient entendus par les acteurs publics. Pour atteindre son objectif qui est « l’Economic Empowerment des populations vulnérables », Tamkeen For Developement aura comme mission de...</p>
                    <a href="/nos-missions" class="learn-more"> La suite </a>
                </div>
            </div>
        </div>
    </section>
    <section class="action-block">
        <div class="container">
            <div class="col-md-12 block text-center">
                <div class="col-md-6 col-md-offset-3 text">
                    <h2>Nos Actions</h2>
                    <p>L’accès aux possibilités et aux ressources économiques peuvent accroître de façon importante les revenus ainsi que les perspectives d’emploi et le développement de petites entreprises. Pour cette fin, « T4D » actionnera plusieurs leviers dont essentiellement...</p>
                    <a href="/nos-actions" class="learn-more"> La suite </a>
                </div>
            </div>
        </div>
    </section>
@endsection
