@extends('layout')

@section('class', $page->type)

@section('content')
    <div class="container">
        <div class="jumbotron">
            <h1>{{ $page->title }}</h1>
            {!! $page->content !!}
        </div>
        <h1></h1>
        <p></p>
    </div>
@endsection
