@extends('layout')

@section('class', 'missions')

@section('content')
    <section class="mission-block">
        <div class="container">
            <div class="col-md-12 block">
                <div class="col-md-6">
                    <br>
                    <h2>Nos Missions</h2>
                    <p>Notre mission est de participer dans l’établissement d’une société civile où les jeunes et les femmes seront capables de se libérer financièrement et de participer activement dans le cycle économique et soient entendus par les acteurs publics.</p>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </section>
    <section class="">
        <div class="container">
            <div class="col-md-8 block">
                <p>Pour atteindre son objectif qui est « l’Economic Empowerment des populations vulnérables », Tamkeen For Developement aura comme mission de :</p>
               <ul class="list-styling">
                    <li>Promouvoir les idées et projets de capacitation économique</li>
                   <li>Préparer la population cible pour l’entreprenariat et l’auto-emploi.</li>
                   <li>Leur assurer l’accès à l’éduction et formations professionnelles nécessaires</li>
                   <li>Les accompagner dans le montage et le démarrage de leurs projets</li>
                   <li>Orienter les responsables publics vers l’amélioration de l’environnement de l’initiative et de l’investissement</li>
                   <li>Permettre l’accès aux ressources financière par le biais de partenariat de coopération</li>
                   <li>Constituer un forum indépendant pour le rapprochement et la convergence des intérêts et la promotion de la coopération entre les différents groupes de la société</li>
                   <li>Faire partie des interlocuteurs privilégiés de l’Etat, des hommes d’affaires et des institutions internationales</li>
               </ul>

                <h3 style="color:#f29216">« T4D » : Un facilitateur de la Transition Démocratique via la Capacitation Economique</h3>
                <p>« T4D » compte jouer un rôle important durant la phase de transition et de consolidation du processus démocratique. Notre programme d’action sera accompli via un partenariat avec le Centre International de Capacitation Économique (CICE) spécialisé dans l’étude et montage de projets de Capacitation Économique.</p>
                L’objectif d’un tel programme est la promotion de l’inclusion socio-économique des populations défavorisées, le renforcement de l’engagement civil ainsi que l’appui de la démocratie participative en implémentant des projets innovateurs. Cela sera fait plus précisément en élaborant des projets où les bénéficiaires seront formés sur l’éducation civique, la citoyenneté et la démocratie participative.</p>
                La pauvreté et l’insécurité financière sont considérées comme barrières à la participation dans la vie politique et l’adhésion aux efforts visant de renforcer la démocratie. La Capacitation Économique va permettre aux populations défavorisées d’avoir de meilleurs revenus, un meilleur accès et contrôle sur leurs ressources ainsi qu’une plus grande sécurité et protection contre la violence. Notre stratégie pour accomplir nos missions se fera à deux niveaux :</p>
                <ul>
                    <li>A l’échelle Micro : « T4D » adressera les problématiques du secteur informel, la structure des marchés, le Leadership, l’Entreprenariat et l’emploi.</li>
                    <li>A l’échelle Macro : A travers un échange participatif entre la société civile, les régulateurs et les corps législatifs afin de soutenir et faciliter l’implémentation d’une gamme de mesures favorisant la création d’un environnement encourageant l’initiative et l’entreprenariat.</li>
                </ul>

                <p>Les principaux résultats d’un tel programme seront le renforcement de la Démocratie via la Capacitation Économique ainsi que l’encouragement de la population et la canalisation de son potentiel vers la coopération et la construction d’une Démocratie prospère.</p>
            </div>
            <div class="col-md-4">
                <img src="{{ asset('img/mission.jpg') }}" alt="">
            </div>
        </div>
    </section>
@endsection
