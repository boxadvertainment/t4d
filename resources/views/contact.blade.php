@extends('layout')

@section('class', 'contact')

@section('content')
    <section class="a-propos-block text-center">
        <div class="container">
            <div class="text-block col-md-8 col-md-offset-2">
                <h2>Nous Contacter</h2>
                <p><b>Pour plus d’informations, veuillez nous contacter sur :</b></p>
                <p>
                    <i class="fa fa-phone"></i> Adresse Mail <a href="mailto:tamkeen@gmail.com">tamkeen@gmail.com</a><br>
                    <i class="fa fa-phone"></i> Tél. :  71 182 008 <br>
                    <i class="fa fa-fax"></i> Fax :  71 182 008 <br>
                </p>
                <p><b>Ou nous visiter dans notre local :</b></p>
                <p>
                    Immeuble Tamkeen, 1ere étage <br>
                    Zone Industrielle  Kheireddine – le Kram 2015 <br>
                    Tunis, Tunisie
                </p>

            </div>
        </div>
    </section>
    <section class="map-block">
        <br>
        <br>
        <center>
        <img src="{{ asset('img/map.jpg') }}" alt=""></center>
        <br>
        <br>
        <br>
    </section>
@endsection
