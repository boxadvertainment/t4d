@extends('layout')

@section('class', 'a-propos')

@section('content')
    <section class="a-propos-block">
        <div class="container">
            <div class="text-block col-md-12">
                <div class="col-md-8">
                    <h2>Qui sommes-nous</h2>
                    <p>Tamkeen for Development « T4D » est une organisation non gouvernementale œuvrant dans la Capacitation Economique des populations vulnérables. A cette fin, elle est appelée à :</p>
                    <ul>
                        <li>Mettre en place des programmes de Capacitation Économique, de formation et d’éducation sur le développement durable, la responsabilité sociale et environnementale.</li>
                        <li>Lutter contre la pauvreté et veiller à la protection de l’environnement et à la défense des droits fondamentaux tout en fournissant l’assistance technique requise.</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <img src="{{ asset('img/member-icon.jpg') }}" alt="">
                </div>

            </div>
        </div>
    </section>
    <section class="members-block">
        <div class="container">
            <img src="{{ asset('img/members.jpg') }}" alt="memebers">
            <br><br><br>
        </div>
    </section>
@endsection
