@extends('layout')

@section('class', 'action')

@section('content')
    <section class="a-propos-block">
        <div class="container">
            <div class="text-block col-md-12">
                <div class="col-md-8">
                    <h2 class="text-center">Nos Actions</h2>
                    <p>L’accès aux possibilités et aux ressources économiques peuvent accroître de façon importante les revenus ainsi que les perspectives d’emploi et le développement de petites entreprises. Pour cette fin, « T4D » actionnera plusieurs leviers dont essentiellement :</p>
                    <ul>
                        <li>Le soutien et la promotion des projets préétablis localement via des partenariats stratégiques touchant parmi d’autres les domaines de l’agriculture, l’industrie agro-alimentaire et les services.</li>
                        <li>La participation à des projets transversaux en partenariat avec des acteurs locaux et internationaux.</li>
                    </ul>
                    <p>« T4D » aura un impact marquant dans trois grandes catégories :</p>
                    <ul>
                        <li>Les projets relatifs aux activités génératrices de revenus tels que les projets d’amélioration de la production agricole, l’appui à la commercialisation des productions locales ainsi que la création de coopératives et de projets communautaires.</li>
                        <li>Les projets de renforcement institutionnel comprenant toute action de sensibilisation, d’encadrement des associations villageoises, de formation, d’établissement de partenariat. La démocratie participative sera présente dans toutes les actions de sensibilisation et d’encadrement.</li>
                        <li>Les projets d’infrastructures sociales de base : ils se résument aux projets relatifs à l’adduction d’eau potable, l’électrification (pour le monde rural), le désenclavement (réalisation des pistes et voies d’accès), construction des écoles et des internats, des maisons de jeunes et d’autres projets d’aménagement ;</li>
                    </ul>
                    <p>L’approche préconisée repose sur la participation et la contribution effective de la population bénéficiaire du projet.</p>
                    <p>Le Centre International de Capacitation Économique (CICE) spécialisé dans l’étude et montage de projets de Capacitation Économique est un des partenaires stratégiques envisagé pour réaliser nos objectifs.</p>
                </div>
                <div class="col-md-4">
                    <br><br><br>
                    <img src="{{ asset('img/action-side.jpg') }}" alt="">
                </div>

            </div>
        </div>
    </section>
@endsection
