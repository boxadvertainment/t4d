<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Page extends Model
{
    /*protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('translation', function(Builder $builder) {
            $locale = \App::getLocale();
            $builder->where('locale', $locale);
        });
    }*/

    public function scopeOfSlug($query, $slug)
    {
        return $query->where('slug', $slug)->where('locale', app()->getLocale());
    }

    public function translation()
    {
        return $this->belongsToMany('Word', 'words', 'id', 'parent_id');
    }

}
