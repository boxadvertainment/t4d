<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function() {
    /*$locale = Request::segment(3);
    app()->setLocale($locale);
    Route::group(['middleware' => ['api'], 'prefix' => 'api/' . app()->getLocale() ], function(){
        Route::get('pages', 'AdminController@pages');
        Route::get('page/{id}', 'AdminController@page');
    });*/

    Route::group(['middleware' => ['api'], 'prefix' => 'api'], function(){
        Route::resource('page', 'PageController');
    });
    Route::get('{admin}', 'AdminController@index')->where('admin', '.*');
});

Route::group(['middleware' => ['web']], function() {

    $locale = Request::segment(1);
    if (in_array($locale, config('app.available_locales'))) {
        app()->setLocale($locale);
    } else {
        $locale = null;
    }

    Route::get('/', 'AppController@home');
    Route::get('/a-propos', 'AppController@propos');
    Route::get('/nos-missions', 'AppController@missions');
    Route::get('/nos-actions', 'AppController@actions');
    Route::get('/nous-contacter', 'AppController@contacter');
    Route::get($locale . '/{slug?}', 'AppController@page')->where('slug', '.*');
    Route::get('{locale?}/{slug?}', ['as' => 'page']);
});


